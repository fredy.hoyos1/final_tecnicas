package dao.impl.list;

import dao.EficienciaDAO;
import dao.exception.CodigoNoPresenteException;
import model.Eficiencia;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class EficienciaDAOList implements EficienciaDAO {

    private static List<Eficiencia> datosEficiencia = new ArrayList<>();


    @Override
    public void registrarEficiencia(Eficiencia eficienciaResivida){

        for (int i = 0; i < datosEficiencia.size(); i++) {
            if (eficienciaResivida.getIdOperario().equals(datosEficiencia.get(i).getIdOperario())) {
                if(eficienciaResivida.getSemana().equals(datosEficiencia.get(i).getSemana())){
                    datosEficiencia.remove(i);
                    datosEficiencia.add(eficienciaResivida);
                    return;

                }
            }
        }
        datosEficiencia.add(eficienciaResivida);
    }

    @Override
    public List<Eficiencia> consultarEficiencia(String idOperario) {
        return datosEficiencia.stream().filter(eficiencia -> eficiencia.getIdOperario().equals(idOperario))
                .collect(Collectors.toList());
    }

    @Override
    public boolean verificarExistenciaSemana(Eficiencia eficienciaPosible)  {
        for (int i = 0; i < datosEficiencia.size(); i++) {
            if (eficienciaPosible.getIdOperario().equals(datosEficiencia.get(i).getIdOperario())) {
                if(eficienciaPosible.getSemana().equals(datosEficiencia.get(i).getSemana())){
                    return true;
                }
            }
        }
       return false;

    }

    @Override
    public double eficienciaSemana(String semana) throws CodigoNoPresenteException {
        int divisor = 0;
        double porcentaje = 0;
        for(int i = 0; i < datosEficiencia.size(); i++){
            if(datosEficiencia.get(i).getSemana().equals(semana)){
                divisor++;
                porcentaje = porcentaje + datosEficiencia.get(i).getPromedio();
            }
        }
        if(divisor > 0){
            return porcentaje/divisor;
        }else {
            throw new CodigoNoPresenteException(semana);
        }
    }

    @Override
    public double eficienciaTotalEmpresa() {
        OptionalDouble promedio = datosEficiencia.stream()
                .mapToDouble(n -> n.getPromedio()).average();
        if(promedio.isPresent()){
            return promedio.getAsDouble();
        }else {
            return 0.0;
        }
    }

}
