package controller;

import bsn.OperarioBSN;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.Eficiencia;
import model.Operario;

import java.util.List;


public class ControllerEficienciaOperario {

    @FXML
    private ComboBox<Operario> cmbOperarios;

    @FXML
    private TableView<Eficiencia> tblEficiencia;
    @FXML
    private TableColumn<Eficiencia, String> clmSemana;
    @FXML
    private TableColumn<Eficiencia, Double> clmLunes;
    @FXML
    private TableColumn<Eficiencia, Double> clmMartes;
    @FXML
    private TableColumn<Eficiencia, Double> clmMiercoles;
    @FXML
    private TableColumn<Eficiencia, Double> clmJueves;
    @FXML
    private TableColumn<Eficiencia, Double> clmViernes;
    @FXML
    private TableColumn<Eficiencia, Double> clmSabado;
    @FXML
    private TableColumn<Eficiencia, Double> clmPromedio;

    @FXML
    private Label txtNombre;
    @FXML
    private Label txtApellido;
    @FXML
    private Label txtIdentificacion;
    @FXML
    private Label txtModulo;
    @FXML
    private Label txtMaquinas;
    @FXML
    private Label txtPromedioTotal;

    private OperarioBSN operarioBSN = new OperarioBSN();

    @FXML
    private void initialize(){
        List<Operario> operarios = operarioBSN.listarOperarios();
        ObservableList<Operario> operarioObservable = FXCollections.observableList(operarios);
        this.cmbOperarios.setItems(operarioObservable);

        clmSemana.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSemana()));
        clmLunes.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getLunes()).asObject());
        clmMartes.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getMartes()).asObject());
        clmMiercoles.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getMiercoles()).asObject());
        clmJueves.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getJueves()).asObject());
        clmViernes.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getViernes()).asObject());
        clmSabado.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getSabado()).asObject());
        clmPromedio.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getPromedio()).asObject());

    }

    public void cmbOperarios_action(){
        Operario operarioSeleccionado = cmbOperarios.getValue();
        if(operarioSeleccionado == null){
            return;
        }
        txtNombre.setText(operarioSeleccionado.getNombre());
        txtApellido.setText(operarioSeleccionado.getApellido());
        txtIdentificacion.setText(operarioSeleccionado.getId());
        if(!operarioSeleccionado.getModulo().equals("")){
            txtModulo.setText(operarioSeleccionado.getModulo());
        }else{
            txtModulo.setText("Sin datos");
        }
        if(!operarioSeleccionado.getMaquinasQueManeja().equals("")){
            txtMaquinas.setText(operarioSeleccionado.getMaquinasQueManeja());
        }else{
            txtMaquinas.setText("Sin datos");
        }

        List<Eficiencia> eficiencias = this.operarioBSN.consultarEficiencia(operarioSeleccionado.getId());
        ObservableList<Eficiencia> eficienciaObservable = FXCollections.observableList(eficiencias);
        tblEficiencia.setItems(eficienciaObservable);

        double promedioAcomulado = this.operarioBSN.obtenerPromedioAcomulado(operarioSeleccionado.getId());
        txtPromedioTotal.setText(String.valueOf(promedioAcomulado));
    }
}
