package controller;

import bsn.OperarioBSN;
import bsn.exception.NoSePuedeEliminarException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import model.Operario;

import java.util.List;

public class ControllerEliminarOperario {

    @FXML
    private ComboBox<Operario> cmbOperarios;

    @FXML
    private Label txtNombre;
    @FXML
    private Label txtApellido;
    @FXML
    private Label txtIdentificacion;
    @FXML
    private Label txtModulo;
    @FXML
    private Label txtMaquinas;

    private OperarioBSN operarioBSN = new OperarioBSN();

    @FXML
    public void initialize(){
        List<Operario> operarios = operarioBSN.listarOperarios();
        ObservableList<Operario> operarioObservable = FXCollections.observableList(operarios);
        this.cmbOperarios.setItems(operarioObservable);
        txtNombre.setText("Sin datos");
        txtApellido.setText("Sin datos");
        txtIdentificacion.setText("Sin datos");
        txtModulo.setText("Sin datos");
        txtMaquinas.setText("Sin datos");
    }

    public void cmbOperarios_action(){
        Operario operarioSeleccionado = cmbOperarios.getValue();
        if(operarioSeleccionado == null){
            return;
        }

        txtNombre.setText(operarioSeleccionado.getNombre());
        txtApellido.setText(operarioSeleccionado.getApellido());
        txtIdentificacion.setText(operarioSeleccionado.getId());
        if(!operarioSeleccionado.getModulo().equals("")){
            txtModulo.setText(operarioSeleccionado.getModulo());
        }else {
            txtModulo.setText("Sin datos");
        }
        if(!operarioSeleccionado.getModulo().equals("")){
            txtMaquinas.setText(operarioSeleccionado.getMaquinasQueManeja());
        }else {
            txtMaquinas.setText("Sin datos");
        }
    }

    public void btnEliminar_action(){
            Operario operarioSeleccionado = cmbOperarios.getValue();
            if(operarioSeleccionado == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eficiencia SARAI.SAS");
                alert.setHeaderText("Error al eliminar");
                alert.setContentText("No hay ningún Operario seleccionado");
                alert.showAndWait();
            }else {
                operarioBSN.eliminarOperario(operarioSeleccionado);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Eficiencia SARAI.SAS");
                alert.setHeaderText("Eliminación exitosa");
                alert.setContentText("El operario "+operarioSeleccionado.getNombre()+" "+operarioSeleccionado.getApellido()+" ah sido\n" +
                        " eliminado");
                alert.showAndWait();
                initialize();
            }
    }
}
