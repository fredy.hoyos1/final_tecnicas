package dao.impl.nio;

import dao.OperarioDAO;
import dao.exception.LlaveDuplicadaException;
import javafx.scene.control.Alert;
import model.Operario;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;


public class OperarioDAONio implements OperarioDAO {

    private final static String NOMBRE_ARCHIVO = "operario";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public OperarioDAONio(){
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }


    @Override
    public void registrarOperario(Operario operario) throws LlaveDuplicadaException {
        Optional<Operario> operarioOptional = this.consultarOperarioPorId(operario.getId());
        if(operarioOptional.isPresent()){
            throw new LlaveDuplicadaException(operario.getId());
        }
        String operarioString = parseOperario2String(operario);
        byte[] datosRegistro = operarioString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try(FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    private String parseOperario2String(Operario operario) {
        StringBuilder sb = new StringBuilder();
        sb.append(operario.getNombre()).append(FIELD_SEPARATOR)
                .append(operario.getApellido()).append(FIELD_SEPARATOR)
                .append(operario.getId()).append(FIELD_SEPARATOR)
                .append(operario.getModulo()).append(FIELD_SEPARATOR)
                .append(operario.getMaquinasQueManeja()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Operario> consultarOperarioPorId(String id) {
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            Optional<String> operarioString = stream.filter(operario -> id.equals(operario.split(",")[2]))
                    .findFirst();
            if(operarioString.isPresent()){
                return  Optional.of(parseOperario2Object(operarioString.get()));
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<Operario> listarOperarios() {
        List<Operario> operarios = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            stream.forEach(operariosString -> operarios.add(parseOperario2Object(operariosString)));
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return operarios;
    }

    private Operario parseOperario2Object(String operariosString) {
        String[] datosOperario = operariosString.split(FIELD_SEPARATOR);
        Operario operario = new Operario(datosOperario[0],
                datosOperario[1], datosOperario[2],
                datosOperario[3],datosOperario[4]);
        return operario;
    }

    @Override
    public void eliminarOperario(Operario operarioSeleccionado) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Eficiencia SARAI.SAS");
        alert.setHeaderText("ELIMINACIÓN CANCELADA");
        alert.setContentText("Esta opción esta disponible en la próxima versión");
        alert.showAndWait();
    }
}
