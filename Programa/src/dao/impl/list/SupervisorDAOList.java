package dao.impl.list;

import dao.SupervisorDAO;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import dao.exception.UltimoEncargadoException;
import model.Operario;
import model.Supervisor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SupervisorDAOList implements SupervisorDAO {

    private static List<Supervisor> datosSupervisor = new ArrayList();

    @Override
    public void registrarSupervisor(Supervisor supervisor) throws LlaveDuplicadaException {
        Optional<Supervisor> supervisorOptional = consultarSupervisorPorId(supervisor.getId());
        if(supervisorOptional.isPresent()){
            throw new LlaveDuplicadaException(supervisor.getId());
        }
        datosSupervisor.add(supervisor);
    }

    @Override
    public Optional<Supervisor> consultarSupervisorPorId(String id){
        return datosSupervisor.stream()
                .filter(supervisor -> supervisor.getId().equals(id))
                .findFirst();
    }

    @Override
    public Optional<Supervisor> consultarSupervisorPorNombre(String nombre) {
        return datosSupervisor.stream()
                .filter(supervisor -> supervisor.getNombre().equals(nombre))
                .findFirst();
    }
    @Override
    public Optional<Supervisor> consultarSupervisorPorContrasena(String contrasena){
        return datosSupervisor.stream()
                .filter(supervisor -> supervisor.getContrasena().equals(contrasena))
                .findFirst();
    }

    @Override
    public void verificarExistenciaSupervisor(Supervisor supervisor) throws LlaveNoExisteException {
        Optional<Supervisor> supervisorOptionalContrasena = consultarSupervisorPorContrasena(supervisor.getContrasena());
        Optional<Supervisor> supervisorOptionalNombre = consultarSupervisorPorNombre(supervisor.getNombre());
        if(supervisorOptionalContrasena.isPresent() && supervisorOptionalNombre.isPresent()){

        }else {
            throw new LlaveNoExisteException(supervisor.getNombre(),supervisor.getContrasena());
        }
    }

    @Override
    public List<Supervisor> listarSupervisores() {
        return new ArrayList(datosSupervisor);
    }

    @Override
    public void eliminarSupervisor(Supervisor supervisor)throws UltimoEncargadoException {
        if(datosSupervisor.size() > 1){
            for (int i = 0; i < datosSupervisor.size(); i++) {
                if (supervisor.getId().equals(datosSupervisor.get(i).getId())){
                    datosSupervisor.remove(i);
                    return;
                }
            }
        }else {
            throw new UltimoEncargadoException(supervisor.getNombre());
        }
    }
}
