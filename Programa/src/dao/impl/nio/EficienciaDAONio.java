package dao.impl.nio;

import dao.EficienciaDAO;
import dao.exception.CodigoNoPresenteException;
import dao.exception.LlaveDuplicadaException;
import javafx.scene.control.Alert;
import model.Eficiencia;
import model.Operario;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class EficienciaDAONio implements EficienciaDAO {

    private final static String NOMBRE_ARCHIVO = "eficiencias";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();


    public EficienciaDAONio(){
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarEficiencia(Eficiencia eficiencia) {
        Optional<Eficiencia> operarioOptional = this.consultarSemanaExiste(eficiencia.getSemana(),eficiencia.getIdOperario());
        if(operarioOptional.isPresent()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Error");
            alert.setContentText("Ya esta registrada esa semana");
            alert.showAndWait();
            return;
        }
        String eficienciaString = parseEficiencia2String(eficiencia);
        byte[] datosRegistro = eficienciaString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try(FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)){
            fileChannel.write(byteBuffer);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    private Optional<Eficiencia> consultarSemanaExiste(String semana, String id) {
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            Optional<String> eficienciaString = stream.filter(eficiencia -> semana.equals(eficiencia.split(",")[7]))
                    .findFirst();
            if(eficienciaString.isPresent()){
                try(Stream<String> stream1 = Files.lines(ARCHIVO)){
                    Optional<String> eficienciaString1 = stream1.filter(eficiencia -> id.equals(eficiencia.split(",")[2]))
                            .findFirst();
                    if(eficienciaString1.isPresent()){
                        return  Optional.of(parseEficiencia2Object(eficienciaString.get()));
                    }
                }catch (IOException ioe){
                    ioe.printStackTrace();
                }
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    private String parseEficiencia2String(Eficiencia eficiencia) {
        StringBuilder sb = new StringBuilder();
        sb.append(eficiencia.getLunes()).append(FIELD_SEPARATOR)
                .append(eficiencia.getMartes()).append(FIELD_SEPARATOR)
                .append(eficiencia.getMiercoles()).append(FIELD_SEPARATOR)
                .append(eficiencia.getJueves()).append(FIELD_SEPARATOR)
                .append(eficiencia.getViernes()).append(FIELD_SEPARATOR)
                .append(eficiencia.getSabado()).append(FIELD_SEPARATOR)
                .append(eficiencia.getDiasTrabajados()).append(FIELD_SEPARATOR)
                .append(eficiencia.getSemana()).append(FIELD_SEPARATOR)
                .append(eficiencia.getIdOperario()).append(RECORD_SEPARATOR);
        return  sb.toString();
    }

    @Override
    public List<Eficiencia> consultarEficiencia(String idOperario) {
        List<Eficiencia> eficiencias = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            eficiencias = stream.filter(eficienciaString -> idOperario.equals(eficienciaString.split(",")[8]))
                    .map(this::parseEficiencia2Object)
                    .collect(Collectors.toList());
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return eficiencias;
    }

    private Eficiencia parseEficiencia2Object(String eficiencaString){
        String[] datosEficiencia = eficiencaString.split(FIELD_SEPARATOR);
        Eficiencia eficiencia = new Eficiencia(Double.parseDouble(datosEficiencia[0]),
                Double.parseDouble(datosEficiencia[1]),Double.parseDouble(datosEficiencia[2]),
                Double.parseDouble(datosEficiencia[3]),Double.parseDouble(datosEficiencia[4]),
                Double.parseDouble(datosEficiencia[5]),Integer.parseInt(datosEficiencia[6]),
                datosEficiencia[7]);
        eficiencia.setIdOperario(datosEficiencia[8]);
        return eficiencia;
    }

    @Override
    public boolean verificarExistenciaSemana(Eficiencia eficienciaPosible) {
        List<Eficiencia> eficiencias = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            eficiencias = stream.filter(eficienciaString -> eficienciaPosible.getIdOperario()
                    .equals(eficienciaString.split(",")[8]))
                    .map(this::parseEficiencia2Object)
                    .collect(Collectors.toList());
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        int tamano = eficiencias.size();
        for(int i = 0; i < tamano; i++){
            if(eficienciaPosible.getSemana().equals(eficiencias.get(i).getSemana())){
                return true;
            }
        }
        return false;
    }

    @Override
    public double eficienciaSemana(String semana) throws CodigoNoPresenteException {
        List<Eficiencia> eficiencias = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            eficiencias = stream.filter(eficienciaString -> semana
                    .equals(eficienciaString.split(",")[7]))
                    .map(this::parseEficiencia2Object)
                    .collect(Collectors.toList());
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        int tamano = eficiencias.size();
        double eficienciaFinal = 0;
        for(int i = 0; i < tamano; i++){
            eficienciaFinal = eficienciaFinal + eficiencias.get(i).getPromedio();
        }
        if(tamano > 0){
            return eficienciaFinal/tamano;
        }else {
            throw new CodigoNoPresenteException(semana);
        }
    }

    @Override
    public double eficienciaTotalEmpresa() {
        List<Eficiencia> eficiencias = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            stream.forEach(eficienciaString -> eficiencias.add(parseEficiencia2Object(eficienciaString)));
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        OptionalDouble promedio = eficiencias.stream()
                .mapToDouble(n -> n.getPromedio()).average();
        if(promedio.isPresent()){
            return promedio.getAsDouble();
        }else {
            return 0.0;
        }
    }
}
