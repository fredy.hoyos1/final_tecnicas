package dao;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import dao.exception.UltimoEncargadoException;
import model.Operario;
import model.Supervisor;

import java.util.List;
import java.util.Optional;

public interface SupervisorDAO {

    void registrarSupervisor(Supervisor supervisor) throws LlaveDuplicadaException;

    Optional<Supervisor> consultarSupervisorPorId(String id);

    Optional<Supervisor> consultarSupervisorPorNombre(String nombre);

    Optional<Supervisor> consultarSupervisorPorContrasena(String contrasena);

    void verificarExistenciaSupervisor(Supervisor supervisor) throws LlaveNoExisteException;

    List<Supervisor> listarSupervisores();

    void eliminarSupervisor(Supervisor supervisor) throws UltimoEncargadoException;
}
