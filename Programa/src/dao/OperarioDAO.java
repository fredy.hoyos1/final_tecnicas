package dao;
import dao.exception.LlaveDuplicadaException;
import model.Operario;

import java.util.List;
import java.util.Optional;

public interface OperarioDAO {

    void registrarOperario(Operario operario) throws LlaveDuplicadaException;

    Optional<Operario> consultarOperarioPorId(String id);

    List<Operario> listarOperarios();

    void eliminarOperario(Operario operarioSeleccionado);
}
