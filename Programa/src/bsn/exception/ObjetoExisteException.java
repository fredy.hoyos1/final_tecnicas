package bsn.exception;

public class ObjetoExisteException extends Exception {

    public ObjetoExisteException(String mensaje){
        super(mensaje);
    }
}
