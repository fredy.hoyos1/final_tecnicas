package dao.exception;

public class LlaveDuplicadaException extends Exception {

    public LlaveDuplicadaException(String llave){
        super(String.format("Ya existe un registro con la llave: %s",llave));
    }
}
