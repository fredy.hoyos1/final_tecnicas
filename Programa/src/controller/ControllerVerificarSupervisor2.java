package controller;

import bsn.SupervisorBSN;
import bsn.exception.ObjetoNoPermitidoException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.Supervisor;

import java.io.IOException;

public class ControllerVerificarSupervisor2 {

    @FXML
    private BorderPane verificarUsuario;
    @FXML
    private TextField txtNombreSupervisor;
    @FXML
    private TextField txtContrasenaSupervisor;


    @FXML
    public void initialize(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Eficiencia SARAI.SAS");
        alert.setHeaderText("opción de ayuda");
        alert.setContentText("SUPERVISOR: Admin \n" +
                "CONTRASEÑA: 123");
        alert.showAndWait();
    }

    public void btnAceptar_action(){
        String nombreVerificar = txtNombreSupervisor.getText();
        String contrasenaVerificar = txtContrasenaSupervisor.getText();
        Supervisor supervisor = new Supervisor(nombreVerificar, null, null,contrasenaVerificar, null);
        SupervisorBSN supervisorBSN = new SupervisorBSN();

        if(nombreVerificar.equals("Admin") && contrasenaVerificar.equals("123")){
            try{
                AnchorPane administrarSupervisor = FXMLLoader
                        .load(getClass().getResource("../view/registrar_eficiencia.fxml"));
                this.verificarUsuario.setCenter(administrarSupervisor);
            }catch (IOException ioe){
                ioe.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eficiencia SARAI.SAS");
                alert.setHeaderText("Error");
                alert.setContentText(ioe.getMessage());
                alert.showAndWait();
            }
        }else{
            try {
                supervisorBSN.verificarRequisitosSupervisor(supervisor);
                AnchorPane administrarSupervisor = FXMLLoader
                        .load(getClass().getResource("../view/registrar_eficiencia.fxml"));
                this.verificarUsuario.setCenter(administrarSupervisor);
            }catch (ObjetoNoPermitidoException | IOException onpe){

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Eficiencia SARAI.SAS");
                alert.setHeaderText("Error de autentificación");
                alert.setContentText("El nombre y/o contraseña es incorrecto");
                alert.showAndWait();
            }
        }



    }
}
