package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ControllerVentanaEficiencia {

    @FXML
    private BorderPane ventanaEficiencia;

    public void btnOperarios_action(){
        try{
            AnchorPane verEficienciaOperario = FXMLLoader
                    .load(getClass().getResource("../view/eficiencia_operario.fxml"));
            this.ventanaEficiencia.setCenter(verEficienciaOperario);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnEmpresa_action(){
        try{
            AnchorPane verEficienciaEmpresa = FXMLLoader
                    .load(getClass().getResource("../view/eficiencia_empresa.fxml"));
            this.ventanaEficiencia.setCenter(verEficienciaEmpresa);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
