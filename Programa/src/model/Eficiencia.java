package model;

public class Eficiencia {

    private double lunes;
    private double martes;
    private double miercoles;
    private double jueves;
    private double viernes;
    private double sabado;
    private double promedio;
    private int diasTrabajados;
    private String semana;


    // relación
    private Operario operario;
    private String idOperario;


    public Eficiencia(double lunes, double martes, double miercoles, double jueves, double viernes, double sabado, int diasTrabajados, String semana) {
        this.lunes = lunes;
        this.martes = martes;
        this.miercoles = miercoles;
        this.jueves = jueves;
        this.viernes = viernes;
        this.sabado = sabado;
        this.semana = semana;
        this.diasTrabajados = diasTrabajados;

        this.promedio = (lunes + martes + miercoles + jueves + viernes + sabado) /diasTrabajados;
    }



    public double getLunes() {
        return lunes;
    }

    public void setLunes(double lunes) {
        this.lunes = lunes;
    }

    public double getMartes() {
        return martes;
    }

    public void setMartes(double martes) {
        this.martes = martes;
    }

    public double getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(double miercoles) {
        this.miercoles = miercoles;
    }

    public double getJueves() {
        return jueves;
    }

    public void setJueves(double jueves) {
        this.jueves = jueves;
    }

    public double getViernes() {
        return viernes;
    }

    public void setViernes(double viernes) {
        this.viernes = viernes;
    }

    public double getSabado() {
        return sabado;
    }

    public void setSabado(double sabado) {
        this.sabado = sabado;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public String getSemana() {
        return semana;
    }

    public void setSemana(String semana) {
        this.semana = semana;
    }

    public double getPromedio() {
        return promedio;
    }

    public Operario getOperario() {
        return operario;
    }

    public void setOperario(Operario operario) {
        this.operario = operario;
    }

    public String getIdOperario() {
        return idOperario;
    }

    public void setIdOperario(String idOperario) {
        this.idOperario = idOperario;
    }


}
