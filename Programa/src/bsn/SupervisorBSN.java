package bsn;
import bsn.exception.NoSePuedeEliminarException;
import bsn.exception.ObjetoExisteException;
import bsn.exception.ObjetoNoPermitidoException;
import dao.SupervisorDAO;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import dao.exception.UltimoEncargadoException;
import dao.impl.nio.SupervisorDAONio;
import model.Supervisor;

import java.util.List;

public class SupervisorBSN {

    private SupervisorDAO supervisorDAO;

    public SupervisorBSN(){
        this.supervisorDAO = new SupervisorDAONio();
    }

    public void registrarSupervisor(Supervisor supervisor) throws ObjetoExisteException {
        try{
            this.supervisorDAO.registrarSupervisor(supervisor);
        }catch (LlaveDuplicadaException lde){
            System.out.println(lde);
            throw new ObjetoExisteException(String
                    .format("El supervisor con identificación: %s ya ha sido registrado",supervisor.getId()));
        }
    }

    public void verificarRequisitosSupervisor(Supervisor supervisor) throws ObjetoNoPermitidoException{
        try {
            this.supervisorDAO.verificarExistenciaSupervisor(supervisor);
        }catch (LlaveNoExisteException lnee){
            System.out.println(lnee);
            throw new ObjetoNoPermitidoException(String
            .format("El supervisor con nombre %s y contraseña %s no concuerdan o no existe",supervisor.getNombre(),"*********"));
        }
    }
    public List<Supervisor> listarSupervisores(){
        return this.supervisorDAO.listarSupervisores();
    }

    public void eliminarSupervisor(Supervisor supervisor)throws NoSePuedeEliminarException {
        try {
            this.supervisorDAO.eliminarSupervisor(supervisor);
        }catch (UltimoEncargadoException nspee){
            System.out.println(nspee);
            throw new NoSePuedeEliminarException(String.format("El supervisor con nombre %s no se puede eliminar\n" +
                    " ya que es el único que esta registrado.",supervisor.getNombre()));
        }

    }

}
