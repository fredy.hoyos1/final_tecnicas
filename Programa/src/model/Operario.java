package model;

import java.util.List;

public class Operario extends Persona {

    private String modulo;
    private String maquinasQueManeja;

    private List<Eficiencia> eficiencia;

    public Operario(String nombre, String apellido, String id, String modulo, String maquinasQueManeja) {
        super(nombre, apellido, id);
        this.modulo = modulo;
        this.maquinasQueManeja = maquinasQueManeja;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getMaquinasQueManeja() {
        return maquinasQueManeja;
    }

    public void setMaquinasQueManeja(String maquinasQueManeja) {
        this.maquinasQueManeja = maquinasQueManeja;
    }

    public List<Eficiencia> getEficiencia() {
        return eficiencia;
    }

    public void setEficiencia(List<Eficiencia> eficiencia) {
        this.eficiencia = eficiencia;
    }

    public String toString(){
        return String.format("%s - %s",this.getId(), this.getNombre());
    }

}
