package bsn.exception;

public class NoSePuedeEliminarException extends Exception {
    public NoSePuedeEliminarException(String mensaje){
        super(mensaje);
    }
}
