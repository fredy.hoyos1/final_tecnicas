package dao;

import dao.exception.CodigoNoPresenteException;
import model.Eficiencia;

import java.util.List;

public interface EficienciaDAO {

    void registrarEficiencia(Eficiencia eficiencia);

    List<Eficiencia> consultarEficiencia(String idOperario);

    boolean verificarExistenciaSemana(Eficiencia eficienciaPosible);

    double eficienciaSemana(String semana) throws CodigoNoPresenteException;

    double eficienciaTotalEmpresa();
}
