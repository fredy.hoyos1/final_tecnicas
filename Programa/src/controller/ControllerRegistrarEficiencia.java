package controller;

import bsn.OperarioBSN;
import dao.EficienciaDAO;
import dao.impl.list.EficienciaDAOList;
import dao.impl.nio.EficienciaDAONio;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Eficiencia;
import model.Operario;

import java.util.List;

public class ControllerRegistrarEficiencia {

    @FXML
    private ComboBox<Operario> cmbOperarios;
    @FXML
    private TextField txtSemana;
    @FXML
    private TextField txtLunes;
    @FXML
    private TextField txtMartes;
    @FXML
    private TextField txtMiercoles;
    @FXML
    private TextField txtJueves;
    @FXML
    private TextField txtViernes;
    @FXML
    private TextField txtSabado;


    @FXML
    private TableView<Eficiencia> tblEficiencia;
    @FXML
    private TableColumn<Eficiencia, String> clmSemana;
    @FXML
    private TableColumn<Eficiencia, Double> clmLunes;
    @FXML
    private TableColumn<Eficiencia, Double> clmMartes;
    @FXML
    private TableColumn<Eficiencia, Double> clmMiercoles;
    @FXML
    private TableColumn<Eficiencia, Double> clmJueves;
    @FXML
    private TableColumn<Eficiencia, Double> clmViernes;
    @FXML
    private TableColumn<Eficiencia, Double> clmSabado;
    @FXML
    private TableColumn<Eficiencia, Double> clmPromedio;



    private OperarioBSN operarioBSN = new OperarioBSN();


    @FXML
    private void initialize(){

        txtSemana.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 3) {
                return change;
            }
            return null;
        }));

        txtLunes.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][1.-9]*)?") && change.getControlNewText().length() <= 6) {
                return change;
            }
            return null;
        }));

        txtMartes.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][1.-9]*)?") && change.getControlNewText().length() <= 6) {
                return change;
            }
            return null;
        }));
        txtMiercoles.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][1.-9]*)?") && change.getControlNewText().length() <= 6) {
                return change;
            }
            return null;
        }));
        txtJueves.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][1.-9]*)?") && change.getControlNewText().length() <= 6) {
                return change;
            }
            return null;
        }));
        txtViernes.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][1.-9]*)?") && change.getControlNewText().length() <= 6) {
                return change;
            }
            return null;
        }));
        txtSabado.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][1.-9]*)?") && change.getControlNewText().length() <= 6) {
                return change;
            }
            return null;
        }));


        List<Operario> operarios = operarioBSN.listarOperarios();
        ObservableList<Operario> operarioObservable = FXCollections.observableList(operarios);
        this.cmbOperarios.setItems(operarioObservable);

        clmSemana.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSemana()));
        clmLunes.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getLunes()).asObject());
        clmMartes.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getMartes()).asObject());
        clmMiercoles.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getMiercoles()).asObject());
        clmJueves.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getJueves()).asObject());
        clmViernes.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getViernes()).asObject());
        clmSabado.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getSabado()).asObject());
        clmPromedio.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getPromedio()).asObject());

    }

    public void cmbOperarios_action(){
        Operario operarioSeleccionado = cmbOperarios.getValue();
        if(operarioSeleccionado == null){
            return;
        }
        List<Eficiencia> eficiencias = this.operarioBSN.consultarEficiencia(operarioSeleccionado.getId());
        ObservableList<Eficiencia> eficienciaObservable = FXCollections.observableList(eficiencias);
        tblEficiencia.setItems(eficienciaObservable);
    }

    public void btnRegistrarEficiencia_action(){

        Operario operarioSeleccionado = cmbOperarios.getValue();

        if(operarioSeleccionado==null){

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de Eficiencia");
            alert.setHeaderText("Registro de eficiencia");
            alert.setContentText("Seleccione un Operario por favor");
            alert.showAndWait();
            return;
        }

        String numeroSemana = txtSemana.getText().trim();
        String eficienciaLunes = txtLunes.getText().trim();
        String eficienciaMartes = txtMartes.getText().trim();
        String eficienciaMiercoles = txtMiercoles.getText().trim();
        String eficienciaJueves = txtJueves.getText().trim();
        String eficienciaViernes = txtViernes.getText().trim();
        String eficienciaSabado = txtSabado.getText().trim();
        boolean trabajo = algunDiaTrabajo(eficienciaLunes,eficienciaMartes,eficienciaMiercoles,eficienciaJueves,eficienciaViernes,eficienciaSabado);
        boolean esValidoSemana = validarCampo(numeroSemana);
        boolean esValidoDouble = validarCamposDouble(eficienciaLunes,eficienciaMartes,eficienciaMiercoles,eficienciaJueves,eficienciaViernes,eficienciaSabado);

        if(!trabajo){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de Eficiencia");
            alert.setHeaderText("Registro de eficiencia");
            alert.setContentText("Para registrar la eficiencia el trabajador debio \n" +
                    "haber trabajado por lo menos un día de la semana");
            alert.showAndWait();
            return;
        }

        if(!esValidoDouble){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de Eficiencia");
            alert.setHeaderText("Registro de eficiencia");
            alert.setContentText("Asegurese que en los días de la semana\n" +
                    "no haya un número mal escrito");
            alert.showAndWait();
            return;
        }

        if(!esValidoSemana){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de Eficiencia");
            alert.setHeaderText("Registro de eficiencia");
            alert.setContentText("Diligencie el campo del número de la semana");
            txtSemana.requestFocus();
            alert.showAndWait();
            return;
        }


        int diasTrabajados = diasTrabajados(eficienciaLunes,eficienciaMartes,eficienciaMiercoles,eficienciaJueves,eficienciaViernes,eficienciaSabado);
        double lunes = 0.0;
        double martes = 0.0;
        double miercoles = 0.0;
        double jueves = 0.0;
        double viernes = 0.0;
        double sabado = 0.0;

        if(!eficienciaLunes.equals("")){
            lunes = Double.parseDouble(eficienciaLunes);
        }
        if(!eficienciaMartes.equals("")){
            martes = Double.parseDouble(eficienciaMartes);
        }
        if(!eficienciaMiercoles.equals("")){
            miercoles = Double.parseDouble(eficienciaMiercoles);
        }
        if(!eficienciaJueves.equals("")){
            jueves = Double.parseDouble(eficienciaJueves);
        }
        if(!eficienciaViernes.equals("")){
            viernes = Double.parseDouble(eficienciaViernes);
        }
        if(!eficienciaSabado.equals("")){
            sabado = Double.parseDouble(eficienciaSabado);
        }


        Eficiencia eficiencia = new Eficiencia(lunes,martes,miercoles,jueves,viernes,sabado,diasTrabajados,numeroSemana);
        OperarioBSN operarioBSN = new OperarioBSN();
        eficiencia.setIdOperario(operarioSeleccionado.getId());
        boolean existeSemana = operarioBSN.existeSemana(eficiencia);
        /*if(existeSemana == true){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de Eficiencia");
            alert.setHeaderText("Renovación de datos");
            alert.setContentText("La semana que selecciono ya existia, y se \n" +
                    "ah renovado los datos de esa semana");
            alert.showAndWait();
        }*/

        if(existeSemana == true){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de Eficiencia");
            alert.setHeaderText("Registro Cancelado");
            alert.setContentText("La semana que selecciono ya ah sido registrada anteriormente");
            alert.showAndWait();
            return;
        }

        limpiarCampos();

        this.operarioBSN.registrarEficiencia(eficiencia);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Registro de Eficiencia");
        alert.setHeaderText("Registro Existoso");
        alert.setContentText("La eficiencia del operario "+operarioSeleccionado.getNombre()+" \n" +
                " ha sido exitoso");
        alert.showAndWait();
    }

    private boolean validarCamposDouble(String... campos){
        for(int i = 0; i < campos.length; i++){
            int muchosPuntos = 0;
            for(int j = 1; j < campos[i].length(); j++){
                if (String.valueOf(campos[i].charAt(j)).equals(".")) {
                    muchosPuntos++;
                }
            }
            if(muchosPuntos > 1){
                return false;
            }
        }
        return true;
    }


    private boolean validarCampo(String campos){
            if(campos == null || "".equals(campos)){
                return false;
            }
        return true;
    }


    private int diasTrabajados(String... dias){
        int diasTrabajados = 6;
        for(int i = 0; i < dias.length; i++){
            if(dias[i] == null || "".equals(dias[i])){
                diasTrabajados--;
            }
        }
        return diasTrabajados;
    }

    public void limpiarCampos(){
        cmbOperarios.setValue(null);
        txtSemana.clear();
        txtLunes.clear();
        txtMartes.clear();
        txtMiercoles.clear();
        txtJueves.clear();
        txtViernes.clear();
        txtSabado.clear();
    }

    public boolean algunDiaTrabajo(String... dias){
        for(int i = 0; i < 6; i++){
            if(!dias[i].equals("")){
                return true;
            }
        }
        return false;
    }


}
