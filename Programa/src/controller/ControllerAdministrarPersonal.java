package controller;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ControllerAdministrarPersonal {

    @FXML
    private BorderPane administrarPersonal;


    public void btnAgregarSupervisor_action(){

        try{
            AnchorPane crearSupervisor = FXMLLoader
                    .load(getClass().getResource("../view/crear_supervisor.fxml"));
            this.administrarPersonal.setCenter(crearSupervisor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnEliminarSupervisor_action(){

        try{
            AnchorPane eliminarSupervisor = FXMLLoader
                    .load(getClass().getResource("../view/eliminar_supervisor.fxml"));
            this.administrarPersonal.setCenter(eliminarSupervisor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnAgregarOperario_action(){

        try{
            AnchorPane crearOperario = FXMLLoader
                    .load(getClass().getResource("../view/crear_operario.fxml"));
            this.administrarPersonal.setCenter(crearOperario);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnEliminarOperario_action(){

        try{
            AnchorPane eliminarOperario = FXMLLoader
                    .load(getClass().getResource("../view/eliminar_operario.fxml"));
            this.administrarPersonal.setCenter(eliminarOperario);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
