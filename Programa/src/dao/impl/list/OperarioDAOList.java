package dao.impl.list;
import dao.OperarioDAO;
import dao.exception.LlaveDuplicadaException;
import model.Operario;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OperarioDAOList implements OperarioDAO {

    public static List<Operario> datosOperario = new ArrayList();

    @Override
    public void registrarOperario(Operario operario) throws LlaveDuplicadaException {
        Optional<Operario> operarioOptional = consultarOperarioPorId(operario.getId());
        if(operarioOptional.isPresent()){
            throw new LlaveDuplicadaException(operario.getId());
        }
        datosOperario.add(operario);

    }

    @Override
    public Optional<Operario> consultarOperarioPorId(String id){
        return datosOperario.stream()
                .filter(operario -> operario.getId().equals(id))
                .findFirst();

    }

    @Override
    public List<Operario> listarOperarios() {
        return new ArrayList<>(datosOperario);
    }

    @Override
    public void eliminarOperario(Operario operarioSeleccionado) {
        for(int i = 0; i < datosOperario.size(); i++){
            if(datosOperario.get(i).getId().equals(operarioSeleccionado.getId())){
                datosOperario.remove(operarioSeleccionado);
                return;
            }
        }
    }
}
