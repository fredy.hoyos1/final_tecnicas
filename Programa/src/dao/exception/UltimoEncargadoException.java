package dao.exception;

public class UltimoEncargadoException extends Exception {
    public UltimoEncargadoException (String nombre){
        super(String.format("El supervisor %s no se puede eliminar ya que es el único registrado en este momento.",nombre));
    }
}
