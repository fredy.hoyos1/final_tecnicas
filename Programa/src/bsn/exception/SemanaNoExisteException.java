package bsn.exception;

public class SemanaNoExisteException extends Exception{
    public SemanaNoExisteException(String mensaje){
        super(mensaje);
    }
}
