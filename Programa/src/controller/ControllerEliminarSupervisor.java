package controller;

import bsn.SupervisorBSN;
import bsn.exception.NoSePuedeEliminarException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import model.Supervisor;

import java.util.List;

public class ControllerEliminarSupervisor {

    @FXML
    private ComboBox<Supervisor> cmbSupervisores;

    @FXML
    private Label txtNombre;
    @FXML
    private Label txtApellido;
    @FXML
    private Label txtIdentificacion;
    @FXML
    private Label txtContrasena;
    @FXML
    private Label txtObservacion;

    private SupervisorBSN supervisorBSN = new SupervisorBSN();

    @FXML
    public void initialize(){
        List<Supervisor> supervisores = supervisorBSN.listarSupervisores();
        ObservableList<Supervisor> supervisorObservable = FXCollections.observableList(supervisores);
        this.cmbSupervisores.setItems(supervisorObservable);
        txtNombre.setText("Sin datos");
        txtApellido.setText("Sin datos");
        txtIdentificacion.setText("Sin datos");
        txtContrasena.setText("Sin datos");
        txtObservacion.setText("Sin datos");
    }


    public void cmbSupervisores_action(){
        Supervisor supervisorSeleccionado = cmbSupervisores.getValue();
        if(supervisorSeleccionado == null){
            return;
        }

        txtNombre.setText(supervisorSeleccionado.getNombre());
        txtApellido.setText(supervisorSeleccionado.getApellido());
        txtIdentificacion.setText(supervisorSeleccionado.getId());
        txtContrasena.setText("*******");
        if(!supervisorSeleccionado.getObservacion().equals("")){
            txtObservacion.setText(supervisorSeleccionado.getObservacion());
        }else{
            txtObservacion.setText("Sin datos");
        }
    }


    public void btnEliminar_action() {
        Supervisor supervisorSeleccionado = cmbSupervisores.getValue();
     if(supervisorSeleccionado == null){
         Alert alert = new Alert(Alert.AlertType.ERROR);
         alert.setTitle("Eficiencia SARAI.SAS");
         alert.setHeaderText("Error de eliminación");
         alert.setContentText("No hay ningún supervisor seleccionado");
         alert.showAndWait();
     }else {
         try {
                 supervisorBSN.eliminarSupervisor(supervisorSeleccionado);
                 Alert alert = new Alert(Alert.AlertType.INFORMATION);
                 alert.setTitle("Eficiencia SARAI.SAS");
                 alert.setHeaderText("Eliminación exitosa");
                 alert.setContentText("El supervisor "+supervisorSeleccionado.getNombre()+" "+supervisorSeleccionado.getApellido()+" ah sido\n" +
                         " eliminado");
                 alert.showAndWait();
                 initialize();

         }catch (NoSePuedeEliminarException nspee){
             Alert alert = new Alert(Alert.AlertType.ERROR);
             alert.setTitle("Eficiencia SARAI.SAS");
             alert.setHeaderText("Error de eliminación");
             alert.setContentText(nspee.getMessage());
             alert.showAndWait();
         }
     }

    }
}
