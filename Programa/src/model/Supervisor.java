package model;

import java.util.List;

public class Supervisor extends Persona  {

    private String contrasena;
    private String observacion;

    private List<Supervisor> supervisors;

    public Supervisor(String nombre, String apellido, String id, String contrasena, String observacion) {
        super(nombre, apellido, id);
        this.contrasena = contrasena;
        this.observacion = observacion;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<Supervisor> getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(List<Supervisor> supervisors) {
        this.supervisors = supervisors;
    }
    public String toString(){
        return String.format("%s - %s",this.getId(), this.getNombre());
    }
}
