package dao.exception;

public class CodigoNoPresenteException extends Exception {
    public CodigoNoPresenteException(String codigo){
        super(String.format("Codigo de semana %s no presente",codigo));
    }
}
