package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ControllerVentanaPrincipal {

    @FXML
    private BorderPane ventanaPrincipal;

    public void empezar_action(){
        try{
            BorderPane contenedorPrincipal = FXMLLoader
                    .load(getClass().getResource("../view/contenedor_principal.fxml"));
            this.ventanaPrincipal.setCenter(contenedorPrincipal);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnIrPantallaPrincipal_action(){
        try{
            BorderPane contenedorPrincipal = FXMLLoader
                    .load(getClass().getResource("../view/contenedor_principal.fxml"));
            this.ventanaPrincipal.setCenter(contenedorPrincipal);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
