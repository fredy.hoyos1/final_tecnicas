package dao.impl.nio;

import dao.SupervisorDAO;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import dao.exception.UltimoEncargadoException;
import javafx.scene.control.Alert;
import model.Operario;
import model.Supervisor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class SupervisorDAONio implements SupervisorDAO {

    private final static String NOMBRE_ARCHIVO = "supervisor";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public SupervisorDAONio(){
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarSupervisor(Supervisor supervisor) throws LlaveDuplicadaException {
        Optional<Supervisor> supervisorOptional = this.consultarSupervisorPorId(supervisor.getId());
        if(supervisorOptional.isPresent()){
            throw new LlaveDuplicadaException(supervisor.getId());
        }
       String supervIsorString = parseSupervisor2String(supervisor);
       byte[] datosRegistro = supervIsorString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try(FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)){
            fileChannel.write(byteBuffer);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    private String parseSupervisor2String(Supervisor supervisor) {
        StringBuilder sb = new StringBuilder();
        sb.append(supervisor.getNombre()).append(FIELD_SEPARATOR)
                .append(supervisor.getApellido()).append(FIELD_SEPARATOR)
                .append(supervisor.getId()).append(FIELD_SEPARATOR)
                .append(supervisor.getContrasena()).append(FIELD_SEPARATOR)
                .append(supervisor.getObservacion()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Supervisor> consultarSupervisorPorId(String id) {
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            Optional<String> supervisorString = stream.filter(supervisor -> id.equals(supervisor.split(",")[2]))
                    .findFirst();
            if(supervisorString.isPresent()){
                return  Optional.of(parseSupervisor2Object(supervisorString.get()));
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    private Supervisor parseSupervisor2Object(String supervisorString) {
        String[] datosSupervisor = supervisorString.split(FIELD_SEPARATOR);
        Supervisor supervisor = new Supervisor(datosSupervisor[0],
                datosSupervisor[1], datosSupervisor[2],
                datosSupervisor[3],datosSupervisor[4]);
        return supervisor;
    }

    @Override
    public Optional<Supervisor> consultarSupervisorPorNombre(String nombre) {
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            Optional<String> supervisorString = stream.filter(supervisor -> nombre.equals(supervisor.split(",")[0]))
                    .findFirst();
            if(supervisorString.isPresent()){
                return  Optional.of(parseSupervisor2Object(supervisorString.get()));
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Supervisor> consultarSupervisorPorContrasena(String contrasena) {
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            Optional<String> supervisorString = stream.filter(supervisor -> contrasena.equals(supervisor.split(",")[3]))
                    .findFirst();
            if(supervisorString.isPresent()){
                return  Optional.of(parseSupervisor2Object(supervisorString.get()));
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public void verificarExistenciaSupervisor(Supervisor supervisor) throws LlaveNoExisteException {
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            Optional<String> supervisorString = stream.filter(revision -> supervisor.getContrasena().equals(revision.split(",")[3]))
                    .findFirst();
            if(supervisorString.isPresent()){
                try(Stream<String> stream1 = Files.lines(ARCHIVO)){
                    Optional<String> supervisorString1 = stream1.filter(revision -> supervisor.getNombre().equals(revision.split(",")[0]))
                            .findFirst();
                    if(supervisorString1.isPresent()){

                    }else {
                        throw new LlaveNoExisteException(supervisor.getNombre(),supervisor.getContrasena());
                    }
                }catch (IOException ioe){
                    throw new LlaveNoExisteException(supervisor.getNombre(),supervisor.getContrasena());
                }
            }else {
                throw new LlaveNoExisteException(supervisor.getNombre(),supervisor.getContrasena());
            }
        }catch (IOException ioe){
            throw new LlaveNoExisteException(supervisor.getNombre(),supervisor.getContrasena());
        }
    }

    @Override
    public List<Supervisor> listarSupervisores() {
        List<Supervisor> supervisores = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            stream.forEach(supervisoresString -> supervisores.add(parseSupervisor2Object(supervisoresString)));
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return supervisores;
    }

    @Override
    public void eliminarSupervisor(Supervisor supervisor) throws UltimoEncargadoException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Eficiencia SARAI.SAS");
        alert.setHeaderText("ELIMINACIÓN CANCELADA");
        alert.setContentText("Esta opción esta disponible en la próxima versión");
        alert.showAndWait();
    }
}
