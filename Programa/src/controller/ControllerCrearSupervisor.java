package controller;

import bsn.SupervisorBSN;
import bsn.exception.ObjetoExisteException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import model.Supervisor;

public class ControllerCrearSupervisor {

    @FXML
    private TextField txtNombreSupervisor;
    @FXML
    private TextField txtApellidoSupervisor;
    @FXML
    private TextField txtIdentificacionSupervisor;
    @FXML
    private TextField txtContrasenaSupervisor;
    @FXML
    private TextField txtObservacionSupervisor;

    private SupervisorBSN supervisorBSN = new SupervisorBSN();

    @FXML
    public void initialize(){
        txtIdentificacionSupervisor.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 12) {
                return change;
            }
            return null;
        }));
    }

    public void btnCrear_action(){
        String nombreSupervisor = txtNombreSupervisor.getText();
        String apellidoSupervisor = txtApellidoSupervisor.getText();
        String identificacionSupervisor = txtIdentificacionSupervisor.getText();
        String contrasenaSupervisor = txtContrasenaSupervisor.getText();
        String observacionSupervisor = txtObservacionSupervisor.getText();

        boolean esValido = validarCampos(nombreSupervisor, apellidoSupervisor, identificacionSupervisor,contrasenaSupervisor);

        if(!esValido){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Registro de Supervisor");
            alert.setContentText("Diligencie los campos de nombre, apellido, identificación y contraseña por favor.");
            alert.showAndWait();
            return;
        }

        if(observacionSupervisor.equals("")){
            observacionSupervisor = "Sin datos";
        }

        Supervisor supervisor = new Supervisor(nombreSupervisor, apellidoSupervisor, identificacionSupervisor,contrasenaSupervisor, observacionSupervisor);
        try {
            supervisorBSN.registrarSupervisor(supervisor);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Registro de Supervisor");
            alert.setContentText("El registro ha sido exitoso.");
            alert.showAndWait();
            limpiarCampos();
        }catch (ObjetoExisteException oyee){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Error");
            alert.setContentText(oyee.getMessage());
            alert.showAndWait();
        }
    }

    private boolean validarCampos(String... campos){
        for(int i = 0; i < campos.length; i++){
            if(campos[i] == null || "".equals(campos[i])){
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtNombreSupervisor.clear();
        txtApellidoSupervisor.clear();
        txtIdentificacionSupervisor.clear();
        txtContrasenaSupervisor.clear();
        txtObservacionSupervisor.clear();
    }
}
