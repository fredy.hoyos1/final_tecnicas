package dao.exception;

public class LlaveNoExisteException extends Exception {

    // esta excepción es en caso de que la verificación de Supervisor no se encuentre en la base de datos
    public LlaveNoExisteException(String nombre, String llave){
        super(String.format("La información con nombre %s  y con contraseña %s no concuerda o no existe", nombre,llave));
    }
}
