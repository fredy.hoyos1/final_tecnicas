package controller;

import bsn.OperarioBSN;
import bsn.exception.SemanaNoExisteException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;


public class ControllerEficienciaEmpresa {

    @FXML
    private Label lblEficienciaSemana;
    @FXML
    private Label lblEficienciaTotal;
    @FXML
    private TextField txtSemana;

    OperarioBSN operarioBSN = new OperarioBSN();


    @FXML
    public void initialize(){
        txtSemana.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 3) {
                return change;
            }
            return null;
        }));
        lblEficienciaTotal.setText("Sin datos");
    }

    public void btnCalcular_action(){
        String semana = txtSemana.getText();

        if("".equals(semana)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia Sarai");
            alert.setHeaderText("Cálculo cancelado");
            alert.setContentText("Por favor elija la semana\n" +
                    "da la cuál quiere saber la eficiencia ");
            alert.showAndWait();
            txtSemana.requestFocus();
            return;
        }

        try {
            lblEficienciaSemana.setText(String.valueOf(operarioBSN.promedioSemana(semana)));
        }catch (SemanaNoExisteException snee){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia Sarai");
            alert.setHeaderText("Cálculo cancelado");
            alert.setContentText(snee.getMessage());
            alert.showAndWait();
            txtSemana.requestFocus();
            return;
        }
        lblEficienciaTotal.setText(String.valueOf(operarioBSN.promedioTotalEmpresa()));

    }

}
