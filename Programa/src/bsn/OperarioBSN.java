package bsn;
import bsn.exception.ObjetoExisteException;
import bsn.exception.SemanaNoExisteException;
import dao.EficienciaDAO;
import dao.OperarioDAO;
import dao.exception.CodigoNoPresenteException;
import dao.exception.LlaveDuplicadaException;
import dao.impl.nio.EficienciaDAONio;
import dao.impl.nio.OperarioDAONio;
import model.Eficiencia;
import model.Operario;

import java.util.List;
import java.util.OptionalDouble;

public class OperarioBSN {

    private OperarioDAO operarioDAO;
    private EficienciaDAO eficienciaDAO;

    public OperarioBSN(){
        this.operarioDAO = new OperarioDAONio();
        this.eficienciaDAO = new EficienciaDAONio();
    }

    public void registrarOperario(Operario operario) throws ObjetoExisteException {

        try {
            this.operarioDAO.registrarOperario(operario);
        }catch (LlaveDuplicadaException lde){
            System.out.println(lde);
            throw new ObjetoExisteException(String
                    .format("El operario con identificación: %s ya ha sido registrado",operario.getId()));
        }

    }

    public void registrarEficiencia(Eficiencia eficiencia){
        this.eficienciaDAO.registrarEficiencia(eficiencia);
    }

    public List<Operario> listarOperarios(){
        return this.operarioDAO.listarOperarios();
    }

    public List<Eficiencia> consultarEficiencia(String idOperario) {
        return this.eficienciaDAO.consultarEficiencia(idOperario);
    }

    public void eliminarOperario(Operario operarioSeleccionado) {
        this.operarioDAO.eliminarOperario(operarioSeleccionado);
    }

    public double obtenerPromedioAcomulado(String id) {
        List<Eficiencia> eficienciasExistentes = this.eficienciaDAO.consultarEficiencia(id);
        OptionalDouble promedio = eficienciasExistentes.stream()
                .mapToDouble(n -> n.getPromedio())
                .average();
        if(promedio.isPresent()){
            return promedio.getAsDouble();
        }else {
            return 0.0;
        }
    }


    public double promedioSemana(String semana) throws SemanaNoExisteException {
        try {
            return eficienciaDAO.eficienciaSemana(semana);
        } catch (CodigoNoPresenteException cnpe){
            System.out.println(cnpe);
            throw new SemanaNoExisteException(String.format("La semana número %s no tiene datos de eficiencia",semana));
        }

    }

    public boolean existeSemana(Eficiencia eficiencia){
        return eficienciaDAO.verificarExistenciaSemana(eficiencia);
    }
    public double promedioTotalEmpresa(){
        return eficienciaDAO.eficienciaTotalEmpresa();
    }
}
