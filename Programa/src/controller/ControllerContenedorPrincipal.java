package controller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import java.io.IOException;

public class ControllerContenedorPrincipal {

    @FXML
    private BorderPane contenedorPrincipal;

    public void btnAdministrarPersonal_action(){

        try{
            BorderPane administrarPersonal = FXMLLoader
                    .load(getClass().getResource("../view/verificar_supervisor.fxml"));
            this.contenedorPrincipal.setCenter(administrarPersonal);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }

    public void btnRegistrarEficiencia_action(){
        try{
            BorderPane administrarPersonal = FXMLLoader
                    .load(getClass().getResource("../view/verificar_supervisor2.fxml"));
            this.contenedorPrincipal.setCenter(administrarPersonal);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }

    public void btnVerEficiencia_action(){

        try{
            BorderPane ventanaEficiencia = FXMLLoader
                    .load(getClass().getResource("../view/ventana_eficiencia.fxml"));
            this.contenedorPrincipal.setCenter(ventanaEficiencia);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnSalir_action(){
        System.exit(0);
    }

}
