package bsn.exception;

public class ObjetoNoPermitidoException extends Exception{

    public ObjetoNoPermitidoException(String mensaje){
        super(mensaje);
    }
}
