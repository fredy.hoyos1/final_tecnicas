package controller;

import bsn.OperarioBSN;
import bsn.exception.ObjetoExisteException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import model.Operario;

public class ControllerCrearOperario {

    @FXML
    private TextField txtNombreOperario;
    @FXML
    private TextField txtApellidoOperario;
    @FXML
    private TextField txtIdentificacionOperario;
    @FXML
    private TextField txtModuloOperario;
    @FXML
    private TextField txtMaquinasOperario;

    OperarioBSN operarioBSN = new OperarioBSN();

    @FXML
    public void initialize(){
        txtIdentificacionOperario.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 12) {
                return change;
            }
            return null;
        }));

        txtModuloOperario.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 2) {
                return change;
            }
            return null;
        }));
    }

    public void btnCrearOperario_action(){

        String nombreOperario = txtNombreOperario.getText();
        String apellidoOperario = txtApellidoOperario.getText();
        String identificacionOperario = txtIdentificacionOperario.getText();
        String moduloOperario = txtModuloOperario.getText();
        String maquinaOperario = txtMaquinasOperario.getText();

        boolean esValido = validarCampos(nombreOperario, apellidoOperario, identificacionOperario);
        if(!esValido){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Registro de Operario");
            alert.setContentText("Diligencie los campo: nombre, apellido e identificación por favor");
            alert.showAndWait();
            return;
        }

        if(moduloOperario.equals("")){
            moduloOperario = "Sin datos";
        }
        if(maquinaOperario.equals("")){
            maquinaOperario = "Sin datos";
        }
        Operario operario = new Operario(nombreOperario, apellidoOperario, identificacionOperario, moduloOperario, maquinaOperario);
        try {
            operarioBSN.registrarOperario(operario);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Registro de Operario");
            alert.setContentText("El registro ha sido exitoso.");
            alert.showAndWait();
            limpiarCampos();
        }catch (ObjetoExisteException oyee){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eficiencia SARAI.SAS");
            alert.setHeaderText("Error");
            alert.setContentText(oyee.getMessage());
            alert.showAndWait();
        }

    }

    public boolean validarCampos(String... campos){
        for(int i = 0; i < campos.length; i++){
            if(campos[i] == null || "".equals(campos[i])){
                return false;
            }
        }
        return true;
    }

    public void limpiarCampos(){
        txtNombreOperario.clear();
        txtApellidoOperario.clear();
        txtIdentificacionOperario.clear();
        txtModuloOperario.clear();
        txtMaquinasOperario.clear();
    }
}
